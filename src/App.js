import React from "react";
import Main from "./components/MainComponent";
import "./style.css";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Main />
      </div>
    );
  }
}

export default App;

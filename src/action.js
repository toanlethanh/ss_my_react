// Action Creator
export function addUser(user) {
    return {
        type: 'ADD_USER',
        data: user
    }
}

